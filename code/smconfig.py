import os
import platform

#MODE='APP'
MODE='OS'

EXEC_PATH = r'D:/keilv5'
CROSS_TOOL = 'keil'

#EXEC_PATH = r'D:\devtools\rttenv\tools\ConEmu\ConEmu\..\..\..\tools\gnu_gcc\arm_gcc\mingw\bin'
#CROSS_TOOL = 'gcc'

TARGET_NAME = "rtthread"

DIST_NAME = "zw1512rm-os"
DIST_ROOT = r"ZWDIST\zw1512rm"

if platform.system().lower() == 'windows':
    DIST_ROOT = os.path.join(os.environ['USERPROFILE'], DIST_ROOT )
elif platform.system().lower() == 'linux':
    DIST_ROOT = os.path.join(os.environ['HOME'], DIST_ROOT )


DIST_PACK_V =  "zw1512rm-os"
DIST_PACK_P = "sos"
DIST_PACK_C = "none"  # compress type allow [quicklz|gzip|none]
DIST_PACK_S = "none"  # crypt type allow [aes|none]
DIST_PACK_I = "none"  # iv for aes-256-cbc 
DIST_PACK_K = "none"  # key for aes-256-cbc

APP_ROOT = os.path.normpath(os.getcwd() )
BSP_ROOT = os.path.normpath(os.getcwd() )
RTT_ROOT = os.path.normpath(os.getcwd() + '/rt-thread/')

RTT_KERNEL_VERSION     = 4
RTT_KERNEL_SUBVERSION  = 1
RTT_KERNEL_REVISION    = 0
