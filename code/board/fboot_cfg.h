#ifndef __FBOOT_CFG_H_
#define __FBOOT_CFG_H_

#include "rtthread.h"
#include "rtdevice.h"
#include "fal.h"

/*
 * fboot option
 */
#define FBOOT_USING_FDOWN         1
#define FBOOT_USING_FCOPY         1
#define FBOOT_USING_FCLEAR        1
#define FBOOT_USING_FUPDATE       1
#define FBOOT_USING_FLOAD         1


#define FBOOT_USING_BOOTLOADER    1
#define FBOOT_USING_BOOTAMAIN     1

#define FBOOT_CUSTOM_JUMP_FUNCTION   1
#define FBOOT_CUSTOM_KEY_STATUS      0


/*
 * fboot select using filesystem
 */
#define FBOOT_USING_FILESYSTEM    1


/*
 * fdown option
 */
#define FDOWN_USING_HTTP          0
#define FDOWN_USING_YMODEM        1

/*
 * fcopy option
 */

/*
 * fclear option
 */


/* fupdate option  */
/**
 * fupdate AES256 encryption algorithm option
 */
#define FUPDATE_USING_AES256     1
/*
 * fupdate decompress mode
 */
#define FUPDATE_USING_FASTLZ     1
#define FUPDATE_USING_QUICKLZ    1
#define FUPDATE_USING_GZIP       1



/* fupdate fal  application partition name */
#define FUPDATE_APP_PART_NAME "app"

/* FOTA download partition name */
#define FUPDATE_DOWN_PART_NAME "down"

#define FUPDATE_AES256_IV "123456789ABCDEF0"
#define FUPDATE_AES256_KEY "123456789ABCDEF0123456789ABCDEF0"

#endif
