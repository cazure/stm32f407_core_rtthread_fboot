/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-01-26     armink       the first version
 */

#include <fal.h>
#ifdef BSP_USING_ON_CHIP_FLASH
#include "drv_flash.h"

static int init(void)
{
    /* do nothing now */
    return 0;
}

static int fal_flash_read_128k(long offset, rt_uint8_t *buf, size_t size);

static int fal_flash_write_128k(long offset, const rt_uint8_t *buf, size_t size);

static int fal_flash_erase_128k(long offset, size_t size);

struct fal_flash_dev onchip_flash =
    {
        .name = FLASH_NAME_ONCHIP, //"onchip"
        .addr = 0x08000000,
        .len = 1024 * 1024,
        .blk_size = 128 * 1024,
        .ops = {
            init,
            fal_flash_read_128k,
            fal_flash_write_128k,
            fal_flash_erase_128k},
        .write_gran = 8};

static int fal_flash_read_128k(long offset, rt_uint8_t *buf, size_t size)
{
    return stm32_flash_read(onchip_flash.addr + offset, buf, size);
}

static int fal_flash_write_128k(long offset, const rt_uint8_t *buf, size_t size)
{
    return stm32_flash_write(onchip_flash.addr + offset, buf, size);
}

static int fal_flash_erase_128k(long offset, size_t size)
{
    return stm32_flash_erase(onchip_flash.addr + offset, size);
}

#endif
