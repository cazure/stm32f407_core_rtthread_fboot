#ifndef _FAL_CFG_H_
#define _FAL_CFG_H_

#include <rtconfig.h>

#define FAL_BOOT_NAME "boot"
#define FAL_BOOT_START 0U
#define FAL_BOOT_SIZE (256 * 1024U)

#define FAL_APP_NAME "app"
#define FAL_APP_START FAL_BOOT_START + FAL_BOOT_SIZE
#define FAL_APP_SIZE (768 * 1024U)

#define FAL_SOS_NAME "sos"
#define FAL_SOS_START FAL_BOOT_START + FAL_BOOT_SIZE
#define FAL_SOS_SIZE (640 * 1024U)

#define FAL_SAPP_NAME "sapp"
#define FAL_SAPP_START FAL_SOS_START + FAL_SOS_SIZE
#define FAL_SAPP_SIZE (128 * 1024U)

#define FAL_DOWN_NAME "down"
#define FAL_DOWN_START 0U
#define FAL_DOWN_SIZE (1024 * 1024U)

#define FAL_FS_NAME "fs"
#define FAL_FS_START FAL_DOWN_START + FAL_DOWN_SIZE
#define FAL_FS_SIZE (15 * 1024 * 1024U)

/* ===================== Flash device Configuration ========================= */

#define FLASH_NAME_ONCHIP "onchip"
#define FLASH_NAME_NOR "W25Q128"

extern struct fal_flash_dev onchip_flash;
extern struct fal_flash_dev nor_flash;

#define FAL_PART_HAS_TABLE_CFG

/* flash device table */
#define FAL_FLASH_DEV_TABLE \
    {                       \
        &onchip_flash,      \
            &nor_flash,     \
    }
/* ====================== Partition Configuration ========================== */

/* partition table */
#define FAL_PART_TABLE                                                                                 \
    {                                                                                                  \
        {FAL_PART_MAGIC_WORD, FAL_BOOT_NAME, FLASH_NAME_ONCHIP, FAL_BOOT_START, FAL_BOOT_SIZE, 0},     \
            {FAL_PART_MAGIC_WORD, FAL_APP_NAME, FLASH_NAME_ONCHIP, FAL_APP_START, FAL_APP_SIZE, 0},    \
            {FAL_PART_MAGIC_WORD, FAL_SOS_NAME, FLASH_NAME_ONCHIP, FAL_SOS_START, FAL_SOS_SIZE, 0},    \
            {FAL_PART_MAGIC_WORD, FAL_SAPP_NAME, FLASH_NAME_ONCHIP, FAL_SAPP_START, FAL_SAPP_SIZE, 0}, \
            {FAL_PART_MAGIC_WORD, FAL_DOWN_NAME, FLASH_NAME_NOR, FAL_DOWN_START, FAL_DOWN_SIZE, 0},    \
            {FAL_PART_MAGIC_WORD, FAL_FS_NAME, FLASH_NAME_NOR, FAL_FS_START, FAL_FS_SIZE, 0},          \
    }

#endif /* _FAL_CFG_H_ */
