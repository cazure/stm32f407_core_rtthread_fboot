#ifndef __LCD_PORT_H__
#define __LCD_PORT_H__

#include "stdint.h"
#include "board.h"

//��Ļ�ͺ�
#define LCD_USE_BTG242432			1

#if (LCD_USE_BTG242432)

#define LCD_WIDTH           320
#define LCD_HEIGHT          240
#define LCD_DPI             166
#define LCD_DIR							0

#define LCD_BITS_PER_PIXEL  16
#define LCD_PIXEL_SIZE      (LCD_BITS_PER_PIXEL / 8)
#define LCD_BUF_SIZE        (LCD_WIDTH * LCD_HEIGHT * LCD_PIXEL_SIZE)
#define LCD_PIXEL_FORMAT    RTGRAPHIC_PIXEL_FORMAT_RGB888

#define LCD_HSYNC_WIDTH     1
#define LCD_VSYNC_HEIGHT    1
#define LCD_HBP             88
#define LCD_VBP             32
#define LCD_HFP             40
#define LCD_VFP             13

#endif

typedef uint16_t lcd_color_t;

struct drv_lcd_device
{
    struct rt_device parent;

    struct rt_device_graphic_info lcd_info;
	
		struct rt_device_graphic_ops lcd_ops;

    struct rt_semaphore lcd_lock;

    /* 0:front_buf is being used 1: back_buf is being used*/
    rt_uint8_t cur_buf;
    rt_uint8_t *front_buf;
    rt_uint8_t *back_buf;
		uint32_t *lcd_buf;
};
#if (LCD_BITS_PER_PIXEL == 24)||(LCD_BITS_PER_PIXEL == 32)

#define WHITE          0xFFFFFF
#define BLACK          0x000000
#define BLUE           0x0000FF
#define BLUE2          0x3F3FFF
#define RED            0xFF0000
#define MAGENTA        0xFF00FF
#define GREEN          0x00FF00
#define CYAN           0x00FFFF
#define YELLOW         0xFFFF00						

#elif (LCD_BITS_PER_PIXEL == 16)

#define WHITE						0xFFFF
#define BLACK						0x0000	  
#define BLUE						0x001F  
#define BRED						0XF81F
#define GRED						0XFFE0
#define GBLUE						0X07FF
#define RED							0xF800
#define MAGENTA					0xF81F
#define GREEN						0x07E0
#define CYAN						0x7FFF
#define YELLOW					0xFFE0
#define BROWN						0XBC40 //��ɫ
#define BRRED						0XFC07 //�غ�ɫ
#define GRAY						0X8430 //��ɫ

#endif

extern void hw_lcd_initial(void);
extern void hw_lcd_delay(int time);
extern void hw_lcd_backlight(int status);
extern void hw_lcd_reset(int status);
//extern void WriteComm(uint16_t CMD);
//extern void WriteData(uint32_t dat);
//extern void LCD_WR_REG(u16 Index,u16 CongfigTemp);

extern void hw_lcd_block(uint16_t Xstart,uint16_t Xend,uint16_t Ystart,uint16_t Yend);
extern void hw_lcd_pixel(uint16_t x,uint16_t y, lcd_color_t Color);
extern void hw_lcd_box(uint16_t xStart,uint16_t yStart,uint16_t xLen,uint16_t yLen,lcd_color_t Color);
extern void hw_lcd_flush(uint16_t xStart,uint16_t yStart,uint16_t xLen,uint16_t yLen,lcd_color_t *buff);
extern void hw_lcd_fill_pic(uint16_t xStart,uint16_t yStart,uint16_t xLen,uint16_t yLen,lcd_color_t *pic_buff);

extern void hw_lcd_char(uint16_t x,uint16_t y,uint8_t num, uint32_t fColor, uint32_t bColor,uint8_t flag);
extern void hw_lcd_string(uint16_t x, uint16_t y, char *s, uint32_t fColor, uint32_t bColor,uint8_t flag);


#endif /* __LCD_PORT_H__ */
