/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-02-23     Malongwei    first version
 */

#ifndef __SRAM_PORT_H__
#define __SRAM_PORT_H__

/* stm32f4 Bank1.sector4 :0X6C000000 */
#define LCD_BANK_ADDR       ((uint32_t)0X6C000000)

/* parameters for sram peripheral */
/* stm32f4 Bank1.sector3 :0X68000000 */
#define SRAM_BANK_ADDR       ((uint32_t)0X68000000)
/* data width: 8, 16, 32 */
#define SRAM_DATA_WIDTH      16
/* sram size */
#define SRAM_SIZE            ((uint32_t)0x00100000)

#endif
