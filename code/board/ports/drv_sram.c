/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-02-23     Malongwei    first version
 */

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

#ifdef BSP_USING_SRAM
#include "drv_sram.h"

#define DRV_DEBUG
#define LOG_TAG             "drv.sram"
#include <drv_log.h>

//void hw_lcd_initial(void);

//void lcd_cmd_btg242432_init(void);

//void lcd_delay_ms(uint16_t ms)
//{
//    rt_thread_mdelay(ms);
//}
//SRAM_HandleTypeDef TFTSRAM_Handler;
////初始化lcd
////该初始化函数可以初始化各种型号的LCD(详见本.c文件最前面的描述)
//void LCD_FMC_Init(void)
//{
//    GPIO_InitTypeDef GPIO_Initure;
//    FSMC_NORSRAM_TimingTypeDef FSMC_ReadWriteTim;
//    FSMC_NORSRAM_TimingTypeDef FSMC_WriteTim;

//    __HAL_RCC_GPIOB_CLK_ENABLE();           //开启GPIOB时钟
//    GPIO_Initure.Pin=GPIO_PIN_15;           //PB15,背光控制
//    GPIO_Initure.Mode=GPIO_MODE_OUTPUT_PP;  //推挽输出
//    GPIO_Initure.Pull=GPIO_PULLUP;          //上拉
//    GPIO_Initure.Speed=GPIO_SPEED_HIGH;     //高速
//    HAL_GPIO_Init(GPIOB,&GPIO_Initure);

//    TFTSRAM_Handler.Instance=FSMC_NORSRAM_DEVICE;
//    TFTSRAM_Handler.Extended=FSMC_NORSRAM_EXTENDED_DEVICE;

//    TFTSRAM_Handler.Init.NSBank=FSMC_NORSRAM_BANK4;                     //使用NE4
//    TFTSRAM_Handler.Init.DataAddressMux=FSMC_DATA_ADDRESS_MUX_DISABLE;  //地址/数据线不复用
//    TFTSRAM_Handler.Init.MemoryType=FSMC_MEMORY_TYPE_SRAM;              //SRAM
//    TFTSRAM_Handler.Init.MemoryDataWidth=FSMC_NORSRAM_MEM_BUS_WIDTH_16; //16位数据宽度
//    TFTSRAM_Handler.Init.BurstAccessMode=FSMC_BURST_ACCESS_MODE_DISABLE; //是否使能突发访问,仅对同步突发存储器有效,此处未用到
//    TFTSRAM_Handler.Init.WaitSignalPolarity=FSMC_WAIT_SIGNAL_POLARITY_LOW;//等待信号的极性,仅在突发模式访问下有用
//    TFTSRAM_Handler.Init.WaitSignalActive=FSMC_WAIT_TIMING_BEFORE_WS;   //存储器是在等待周期之前的一个时钟周期还是等待周期期间使能NWAIT
//    TFTSRAM_Handler.Init.WriteOperation=FSMC_WRITE_OPERATION_ENABLE;    //存储器写使能
//    TFTSRAM_Handler.Init.WaitSignal=FSMC_WAIT_SIGNAL_DISABLE;           //等待使能位,此处未用到
//    TFTSRAM_Handler.Init.ExtendedMode=FSMC_EXTENDED_MODE_ENABLE;        //读写使用不同的时序
//    TFTSRAM_Handler.Init.AsynchronousWait=FSMC_ASYNCHRONOUS_WAIT_DISABLE;//是否使能同步传输模式下的等待信号,此处未用到
//    TFTSRAM_Handler.Init.WriteBurst=FSMC_WRITE_BURST_DISABLE;           //禁止突发写
//    TFTSRAM_Handler.Init.ContinuousClock=FSMC_CONTINUOUS_CLOCK_SYNC_ASYNC;
//		
////		TFTSRAM_Handler.Init.NSBank = FSMC_NORSRAM_BANK4;
////		TFTSRAM_Handler.Init.DataAddressMux = FSMC_DATA_ADDRESS_MUX_DISABLE;
////		TFTSRAM_Handler.Init.MemoryType = FSMC_MEMORY_TYPE_SRAM;
////		TFTSRAM_Handler.Init.MemoryDataWidth = FSMC_NORSRAM_MEM_BUS_WIDTH_16;
////		TFTSRAM_Handler.Init.BurstAccessMode = FSMC_BURST_ACCESS_MODE_DISABLE;
////		TFTSRAM_Handler.Init.WaitSignalPolarity = FSMC_WAIT_SIGNAL_POLARITY_LOW;
//		TFTSRAM_Handler.Init.WrapMode = FSMC_WRAP_MODE_DISABLE;
////		TFTSRAM_Handler.Init.WaitSignalActive = FSMC_WAIT_TIMING_BEFORE_WS;
////		TFTSRAM_Handler.Init.WriteOperation = FSMC_WRITE_OPERATION_ENABLE;
////		TFTSRAM_Handler.Init.WaitSignal = FSMC_WAIT_SIGNAL_DISABLE;
////		TFTSRAM_Handler.Init.ExtendedMode = FSMC_EXTENDED_MODE_DISABLE;
////		TFTSRAM_Handler.Init.AsynchronousWait = FSMC_ASYNCHRONOUS_WAIT_DISABLE;
////		TFTSRAM_Handler.Init.WriteBurst = FSMC_WRITE_BURST_DISABLE;
//		TFTSRAM_Handler.Init.PageSize = FSMC_PAGE_SIZE_NONE;

//    //FMC读时序控制寄存器
//    FSMC_ReadWriteTim.AddressSetupTime=0x0F;        //地址建立时间（ADDSET）为16个HCLK 1/168M=6ns*16=96ns
//    FSMC_ReadWriteTim.AddressHoldTime=0;
//    FSMC_ReadWriteTim.DataSetupTime=60;             //数据保存时间为60个HCLK    =6*60=360ns
//    FSMC_ReadWriteTim.AccessMode=FSMC_ACCESS_MODE_A;//模式A
//    //FMC写时序控制寄存器
//    FSMC_WriteTim.BusTurnAroundDuration=0;          //总线周转阶段持续时间为0，此变量不赋值的话会莫名其妙的自动修改为4。导致程序运行正常
//    FSMC_WriteTim.AddressSetupTime=9;               //地址建立时间（ADDSET）为9个HCLK =54ns
//    FSMC_WriteTim.AddressHoldTime=0;
//    FSMC_WriteTim.DataSetupTime=8;                  //数据保存时间为6ns*9个HCLK=54n
//    FSMC_WriteTim.AccessMode=FSMC_ACCESS_MODE_A;    //模式A

////    HAL_SRAM_MspInit_ADD(&TFTSRAM_Handler);

//    HAL_SRAM_Init(&TFTSRAM_Handler,&FSMC_ReadWriteTim,&FSMC_WriteTim);

//    lcd_delay_ms(50); // delay 50 ms
//    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET);

//		lcd_cmd_btg242432_init();
//}



static SRAM_HandleTypeDef hsram2;

static FSMC_NORSRAM_TimingTypeDef Timing1 = {0};
static FSMC_NORSRAM_TimingTypeDef Timing2 = {0};

static int rt_hw_lcd_init(void)
{
	int result = RT_EOK;
  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

    GPIO_InitTypeDef GPIO_Initure;
	
    __HAL_RCC_GPIOB_CLK_ENABLE();           //开启GPIOB时钟
    GPIO_Initure.Pin=GPIO_PIN_15;           //PB15,背光控制
    GPIO_Initure.Mode=GPIO_MODE_OUTPUT_PP;  //推挽输出
    GPIO_Initure.Pull=GPIO_PULLUP;          //上拉
    GPIO_Initure.Speed=GPIO_SPEED_HIGH;     //高速
    HAL_GPIO_Init(GPIOB,&GPIO_Initure);

//	FSMC_NORSRAM_TimingTypeDef Timing = {0};
  /** Perform the SRAM2 memory initialization sequence
  */
  hsram2.Instance = FSMC_NORSRAM_DEVICE;
  hsram2.Extended = FSMC_NORSRAM_EXTENDED_DEVICE;
  /* hsram2.Init */
  hsram2.Init.NSBank = FSMC_NORSRAM_BANK4;
  hsram2.Init.DataAddressMux = FSMC_DATA_ADDRESS_MUX_DISABLE;
  hsram2.Init.MemoryType = FSMC_MEMORY_TYPE_SRAM;
  hsram2.Init.MemoryDataWidth = FSMC_NORSRAM_MEM_BUS_WIDTH_16;
  hsram2.Init.BurstAccessMode = FSMC_BURST_ACCESS_MODE_DISABLE;
  hsram2.Init.WaitSignalPolarity = FSMC_WAIT_SIGNAL_POLARITY_LOW;
  hsram2.Init.WrapMode = FSMC_WRAP_MODE_DISABLE;
  hsram2.Init.WaitSignalActive = FSMC_WAIT_TIMING_BEFORE_WS;
  hsram2.Init.WriteOperation = FSMC_WRITE_OPERATION_ENABLE;
  hsram2.Init.WaitSignal = FSMC_WAIT_SIGNAL_DISABLE;
  hsram2.Init.ExtendedMode = FSMC_EXTENDED_MODE_DISABLE;
  hsram2.Init.AsynchronousWait = FSMC_ASYNCHRONOUS_WAIT_DISABLE;
  hsram2.Init.WriteBurst = FSMC_WRITE_BURST_DISABLE;
  hsram2.Init.PageSize = FSMC_PAGE_SIZE_NONE;
	
	hsram2.Init.ContinuousClock = FSMC_CONTINUOUS_CLOCK_SYNC_ASYNC;
	
  /* Timing */
  Timing1.AddressSetupTime = 0x0f;
  Timing1.AddressHoldTime = 0;
  Timing1.DataSetupTime = 60;
  Timing1.BusTurnAroundDuration = 0;
  Timing1.CLKDivision = 0;
  Timing1.DataLatency = 0;
  Timing1.AccessMode = FSMC_ACCESS_MODE_A;
  /* Timing */
  Timing2.AddressSetupTime = 0x09;
  Timing2.AddressHoldTime = 0;
  Timing2.DataSetupTime = 0x08;
  Timing2.BusTurnAroundDuration = 0;
  Timing2.CLKDivision = 0;
  Timing2.DataLatency = 0;
  Timing2.AccessMode = FSMC_ACCESS_MODE_A;
	
  /* ExtTiming */
	if (HAL_SRAM_Init(&hsram2, &Timing1, &Timing2) != HAL_OK)
	{
			LOG_E("LCD init failed!");
			result = -RT_ERROR;
	}
	//hw_lcd_initial();
	return result;
}
INIT_BOARD_EXPORT(rt_hw_lcd_init);


//void lcdsetconfig(int array[6])
//{
//	HAL_SRAM_DeInit(&hsram2);
//	
//  hsram2.Instance = FSMC_NORSRAM_DEVICE;
//  hsram2.Extended = FSMC_NORSRAM_EXTENDED_DEVICE;
//  /* hsram2.Init */
//  hsram2.Init.NSBank = FSMC_NORSRAM_BANK4;
//  hsram2.Init.DataAddressMux = FSMC_DATA_ADDRESS_MUX_DISABLE;
//  hsram2.Init.MemoryType = FSMC_MEMORY_TYPE_SRAM;
//  hsram2.Init.MemoryDataWidth = FSMC_NORSRAM_MEM_BUS_WIDTH_16;
//  hsram2.Init.BurstAccessMode = FSMC_BURST_ACCESS_MODE_DISABLE;
//  hsram2.Init.WaitSignalPolarity = FSMC_WAIT_SIGNAL_POLARITY_LOW;
//  hsram2.Init.WrapMode = FSMC_WRAP_MODE_DISABLE;
//  hsram2.Init.WaitSignalActive = FSMC_WAIT_TIMING_BEFORE_WS;
//  hsram2.Init.WriteOperation = FSMC_WRITE_OPERATION_ENABLE;
//  hsram2.Init.WaitSignal = FSMC_WAIT_SIGNAL_DISABLE;
//  hsram2.Init.ExtendedMode = FSMC_EXTENDED_MODE_DISABLE;
//  hsram2.Init.AsynchronousWait = FSMC_ASYNCHRONOUS_WAIT_DISABLE;
//  hsram2.Init.WriteBurst = FSMC_WRITE_BURST_DISABLE;
//  hsram2.Init.PageSize = FSMC_PAGE_SIZE_NONE;
//	
//  Timing.AddressSetupTime = array[0];
//  Timing.AddressHoldTime = array[1];
//  Timing.DataSetupTime = array[2];
//  Timing.BusTurnAroundDuration = array[3];
//  Timing.CLKDivision = array[4];
//  Timing.DataLatency = array[5];
//  Timing.AccessMode = FSMC_ACCESS_MODE_A;
//	if (HAL_SRAM_Init(&hsram2, &Timing, NULL) != HAL_OK)
//	{
//			LOG_E("LCD init failed!");
//	}
//}


//int lcdconfig(int argc, char **argv)
//{
//	int array[6] = {0};
//	if(argc > 6)
//	{
//		array[0] = atoi(argv[1]);
//		array[1] = atoi(argv[2]);
//		array[2] = atoi(argv[3]);
//		array[3] = atoi(argv[4]);
//		array[4] = atoi(argv[5]);
//		array[5] = atoi(argv[6]);
//		rt_kprintf("lcdconfig : %d %d %d %d %d %d\n", array[0], array[1], array[2], array[3], array[4], array[5]);
//		lcdsetconfig(array);
//		hw_lcd_initial();
//	}
//	return 0;
//}
//MSH_CMD_EXPORT(lcdconfig, lcd config)



#ifdef RT_USING_MEMHEAP_AS_HEAP
static struct rt_memheap system_heap;
#endif

static SRAM_HandleTypeDef hsram;

static int rt_hw_sram_init(void)
{
    int result = RT_EOK;

    FSMC_NORSRAM_TimingTypeDef Timing = {0};

    /** Perform the SRAM2 memory initialization sequence
    */
    hsram.Instance = FSMC_NORSRAM_DEVICE;
    hsram.Extended = FSMC_NORSRAM_EXTENDED_DEVICE;
    /* hsram.Init */
    hsram.Init.NSBank = FSMC_NORSRAM_BANK3;
    hsram.Init.DataAddressMux = FSMC_DATA_ADDRESS_MUX_DISABLE;
    hsram.Init.MemoryType = FSMC_MEMORY_TYPE_SRAM;
#if SRAM_DATA_WIDTH == 8
    hsram.Init.MemoryDataWidth = FSMC_NORSRAM_MEM_BUS_WIDTH_8;
#elif SRAM_DATA_WIDTH == 16
    hsram.Init.MemoryDataWidth = FSMC_NORSRAM_MEM_BUS_WIDTH_16;
#else
    hsram.Init.MemoryDataWidth = FSMC_NORSRAM_MEM_BUS_WIDTH_32;
#endif
    hsram.Init.BurstAccessMode = FSMC_BURST_ACCESS_MODE_DISABLE;
    hsram.Init.WaitSignalPolarity = FSMC_WAIT_SIGNAL_POLARITY_LOW;
    hsram.Init.WrapMode = FSMC_WRAP_MODE_DISABLE;
    hsram.Init.WaitSignalActive = FSMC_WAIT_TIMING_BEFORE_WS;
    hsram.Init.WriteOperation = FSMC_WRITE_OPERATION_ENABLE;
    hsram.Init.WaitSignal = FSMC_WAIT_SIGNAL_DISABLE;
    hsram.Init.ExtendedMode = FSMC_EXTENDED_MODE_DISABLE;
    hsram.Init.AsynchronousWait = FSMC_ASYNCHRONOUS_WAIT_DISABLE;
    hsram.Init.WriteBurst = FSMC_WRITE_BURST_DISABLE;
    hsram.Init.PageSize = FSMC_PAGE_SIZE_NONE;


    /* Timing */
    Timing.AddressSetupTime = 0;
    Timing.AddressHoldTime = 0;
    Timing.DataSetupTime = 8;
    Timing.BusTurnAroundDuration = 0;
    Timing.CLKDivision = 0;
    Timing.DataLatency = 0;
    Timing.AccessMode = FSMC_ACCESS_MODE_A;
    /* ExtTiming */

    if (HAL_SRAM_Init(&hsram, &Timing, &Timing) != HAL_OK)
    {
        LOG_E("SRAM init failed!");
        result = -RT_ERROR;
    }
    else
    {
        LOG_D("sram init success, mapped at 0x%X, size is %d bytes, data width is %d", SRAM_BANK_ADDR, SRAM_SIZE, SRAM_DATA_WIDTH);
#ifdef RT_USING_MEMHEAP_AS_HEAP
        /* If RT_USING_MEMHEAP_AS_HEAP is enabled, SRAM is initialized to the heap */
        rt_memheap_init(&system_heap, "sram", (void *)SRAM_BANK_ADDR, SRAM_SIZE);
#endif
    }

    return result;
}
INIT_BOARD_EXPORT(rt_hw_sram_init);

//#ifdef DRV_DEBUG
//#ifdef FINSH_USING_MSH
//static int sram_test(void)
//{
//    int i = 0;
//    uint32_t start_time = 0, time_cast = 0;
//#if SRAM_DATA_WIDTH == 8
//    char data_width = 1;
//    uint8_t data = 0;
//#elif SRAM_DATA_WIDTH == 16
//    char data_width = 2;
//    uint16_t data = 0;
//#else
//    char data_width = 4;
//    uint32_t data = 0;
//#endif

//    /* write data */
//    LOG_D("Writing the %ld bytes data, waiting....", SRAM_SIZE);
//    start_time = rt_tick_get();
//    for (i = 0; i < SRAM_SIZE / data_width; i++)
//    {
//#if SRAM_DATA_WIDTH == 8
//        *(__IO uint8_t *)(SRAM_BANK_ADDR + i * data_width) = (uint8_t)0x55;
//#elif SRAM_DATA_WIDTH == 16
//        *(__IO uint16_t *)(SRAM_BANK_ADDR + i * data_width) = (uint16_t)0x5555;
//#else
//        *(__IO uint32_t *)(SRAM_BANK_ADDR + i * data_width) = (uint32_t)0x55555555;
//#endif
//    }
//    time_cast = rt_tick_get() - start_time;
//    LOG_D("Write data success, total time: %d.%03dS.", time_cast / RT_TICK_PER_SECOND,
//          time_cast % RT_TICK_PER_SECOND / ((RT_TICK_PER_SECOND * 1 + 999) / 1000));

//    /* read data */
//    LOG_D("start Reading and verifying data, waiting....");
//    for (i = 0; i < SRAM_SIZE / data_width; i++)
//    {
//#if SRAM_DATA_WIDTH == 8
//        data = *(__IO uint8_t *)(SRAM_BANK_ADDR + i * data_width);
//        if (data != 0x55)
//        {
//            LOG_E("SRAM test failed!");
//            break;
//        }
//#elif SRAM_DATA_WIDTH == 16
//        data = *(__IO uint16_t *)(SRAM_BANK_ADDR + i * data_width);
//        if (data != 0x5555)
//        {
//            LOG_E("SRAM test failed!");
//            break;
//        }
//#else
//        data = *(__IO uint32_t *)(SRAM_BANK_ADDR + i * data_width);
//        if (data != 0x55555555)
//        {
//            LOG_E("SRAM test failed!");
//            break;
//        }
//#endif
//    }

//    if (i >= SRAM_SIZE / data_width)
//    {
//        LOG_D("SRAM test success!");
//    }

//    return RT_EOK;
//}
//MSH_CMD_EXPORT(sram_test, sram test);
//#endif /* FINSH_USING_MSH */
//#endif /* DRV_DEBUG */
#endif /* BSP_USING_SRAM */
