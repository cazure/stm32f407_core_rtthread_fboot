/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-01-26     armink       the first version
 */
#include <fal.h>
#include <sfud.h>

#define FAL_USING_SFUD_PORT
#ifdef FAL_USING_SFUD_PORT
#ifdef RT_USING_SFUD
#include <spi_flash_sfud.h>
#endif

static int flash0_init(void);
static int flash0_read(long offset, uint8_t *buf, size_t size);
static int flash0_write(long offset, const uint8_t *buf, size_t size);
static int flash0_erase(long offset, size_t size);

static sfud_flash_t sfud_dev = NULL;
struct fal_flash_dev nor_flash =
    {
        .name = FLASH_NAME_NOR, //"norflash"
        .addr = 0,
        .len = 16 * 1024 * 1024,
        .blk_size = 4096,
        .ops = {flash0_init, flash0_read, flash0_write, flash0_erase},
        .write_gran = 1};

static int flash0_init(void)
{
#ifdef RT_USING_SFUD
    /* RT-Thread RTOS platform */
    sfud_dev = rt_sfud_flash_find_by_dev_name(FLASH_NAME_NOR);
#else
    /* bare metal platform */
    extern sfud_flash sfud_norflash0;
    sfud_dev = &sfud_norflash0;
#endif

    if (NULL == sfud_dev)
    {
        return -1;
    }
    /* update the flash chip information */
    nor_flash.blk_size = sfud_dev->chip.erase_gran;
    nor_flash.len = sfud_dev->chip.capacity;
    return 0;
}

static int flash0_read(long offset, uint8_t *buf, size_t size)
{
    assert(sfud_dev);
    assert(sfud_dev->init_ok);
    sfud_read(sfud_dev, nor_flash.addr + offset, size, buf);

    return size;
}

static int flash0_write(long offset, const uint8_t *buf, size_t size)
{
    assert(sfud_dev);
    assert(sfud_dev->init_ok);
    if (sfud_write(sfud_dev, nor_flash.addr + offset, size, buf) != SFUD_SUCCESS)
    {
        return -1;
    }

    return size;
}

static int flash0_erase(long offset, size_t size)
{
    assert(sfud_dev);
    assert(sfud_dev->init_ok);
    if (sfud_erase(sfud_dev, nor_flash.addr + offset, size) != SFUD_SUCCESS)
    {
        return -1;
    }

    return size;
}
#endif /* FAL_USING_SFUD_PORT */
