#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

int board_led_set(int index, int status);

int board_system_init(void);
int board_flash_init(void);
int board_filesystem_init(void);
void bootloader_fboot_main(void);

int borad_env_init(void)
{
	board_flash_init();

	board_filesystem_init();

	board_system_init();
	
	board_led_set(0, 0); // err
	board_led_set(1, 1); // run

	bootloader_fboot_main();
	return RT_EOK;
}
INIT_ENV_EXPORT(borad_env_init);


int main(void)
{
	while (1)
	{
		board_led_set(0, 1);
		board_led_set(1, 1);
		rt_thread_mdelay(300);
		board_led_set(0, 0);
		board_led_set(1, 0);
		rt_thread_mdelay(300);
	}
}
