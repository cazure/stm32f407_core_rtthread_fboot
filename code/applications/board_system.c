/*
 * Change Logs:
 * Date           Author       Notes
 * 2020-10-15     chenbin
 * 2020-11-13     chenbin
 */
#include "board.h"
#include "fboot.h"
#include "board_system.h"

typedef union
{
	uint32_t u32[4];
	char u8[16];
} uid_data_t;

static char _unique_id_str[64] = {0};

char *board_unique_id_str(void)
{
	return _unique_id_str;
}

static char board_version_str[64] = {0};

char *board_version(void)
{
    return board_version_str;
}

/*
* 
* http://patorjk.com/software/taag/#p=display&h=0&v=0&c=vb&f=Big&t=FBOOT
*/

const char *dev_logo = "\n\n\n\
   ______   ____     ____     ____    _______  \n\
  |  ____| |  _ \\   / __ \\   / __ \\  |__   __|  \n\
  | |__    | |_) | | |  | | | |  | |    | |     \n\
  |  __|   |  _ <  | |  | | | |  | |    | |     \n\
  | |      | |_) | | |__| | | |__| |    | |     \n\
  |_|      |____/   \\____/   \\____/     |_|     \n\
";



void board_show_logo(void)
{
	rt_kputs(dev_logo);
}

void board_show_info(void)
{
	uid_data_t data;
	data.u32[0] = HAL_GetUIDw0();
	data.u32[1] = HAL_GetUIDw1();
	data.u32[2] = HAL_GetUIDw2();
	data.u32[3] = 0;
	rt_snprintf(_unique_id_str, 64, "%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
							data.u8[0], data.u8[1], data.u8[2], data.u8[3],
							data.u8[4], data.u8[5], data.u8[6], data.u8[7],
							data.u8[8], data.u8[9], data.u8[10], data.u8[11]);
	
	rt_snprintf(board_version_str,64,"%s","20220101");
	
	rt_kprintf("VER: %s\n",board_version_str);
	rt_kprintf("UID: %s\n", _unique_id_str);
	rt_kprintf("build time: %s %s\n", __DATE__, __TIME__);
}

// void board_load_default_mac(uint8_t* mac_addr)
//{
//	uid_data_t data;
//	data.u32[0] = HAL_GetUIDw0();
//	data.u32[1] = HAL_GetUIDw1();
//	data.u32[2] = HAL_GetUIDw2();
//	data.u32[3] = 0;
//
//	/* OUI 00-80-E1 STMICROELECTRONICS. */
//	mac_addr[0] = 0x00;
//	mac_addr[1] = 0x80;
//	mac_addr[2] = 0xE1;
//
////	/* NXP (Freescale) MAC OUI */
////	mac_addr[0] = 0x00;
////	mac_addr[1] = 0x04;
////	mac_addr[2] = 0x49;
//
//  /* generate MAC addr from 96bit unique ID (only for test). */
//	mac_addr[3] = data.u32[2];
//	mac_addr[4] = data.u32[1];
//	mac_addr[5] = data.u32[0];
//
//}

//#include "board_config.h"
// static userconfig_system_t system_config = {0};

// userconfig_system_t * board_system_get(void)
//{
//     return &(system_config);
// }

// char *board_hostname(void)
//{
//     return system_config.name;
// }


#include "drv_gpio.h"
/* defined the KEYUP pin: PA) */
#define KEYUP_PIN GET_PIN(A, 0)

/* defined the KEYUP pin: PA) */
#define KEY0_PIN GET_PIN(E, 4)

/* defined the LED0 pin: PF9 */
#define LED0_PIN GET_PIN(F, 9)

/* defined the LED1 pin: PF10 */
#define LED1_PIN GET_PIN(F, 10)

int board_led_set(int index, int status)
{
	switch (index)
	{
	case 0:
	{
		if (status)
			rt_pin_write(LED0_PIN, PIN_LOW);
		else
			rt_pin_write(LED0_PIN, PIN_HIGH);
	}
	break;
	case 1:
	{
		if (status)
			rt_pin_write(LED1_PIN, PIN_LOW);
		else
			rt_pin_write(LED1_PIN, PIN_HIGH);
	}
	break;
	}
	return 0;
}

int board_key_get(int index)
{
	switch (index)
	{
	case 0:
	{
		return rt_pin_read(KEY0_PIN);
	}
	case 1:
	{
		return rt_pin_read(KEYUP_PIN);
	}
	}
	return 0;
}

int board_system_init(void)
{
	/* set LED0 pin mode to output */
	rt_pin_mode(LED0_PIN, PIN_MODE_OUTPUT);
	rt_pin_mode(LED1_PIN, PIN_MODE_OUTPUT);

	/* set KEY pin mode to output */
	rt_pin_mode(KEYUP_PIN, PIN_MODE_INPUT_PULLDOWN);
	rt_pin_mode(KEY0_PIN, PIN_MODE_INPUT_PULLDOWN);

	rt_pin_write(LED0_PIN, PIN_LOW);
	rt_pin_write(LED1_PIN, PIN_LOW);

	// userconfig_system_load(&(system_config));

	return RT_EOK;
}

// void rt_hw_us_delay(rt_uint32_t us)
//{
//	while(us--);
// }

void delay_us(uint32_t us)
{
	rt_hw_us_delay(us);
}

void delay_ms(uint32_t ms)
{
	rt_thread_mdelay(ms);
}

uint32_t tick_get(void)
{
	return rt_tick_get();
}

#ifdef RT_USING_FINSH
#include <finsh.h>
#include <rthw.h>
 static void reboot(uint8_t argc, char **argv)
{
	delay_ms(1000);
	rt_kprintf("reboot ...\n");
	delay_ms(100);
	rt_hw_cpu_reset();
}
MSH_CMD_EXPORT_ALIAS(reboot, reboot, Reboot System);
#endif /* RT_USING_FINSH */

void __aeabi_memcpy(void *dest, const void *src, size_t n)
{
	rt_memcpy(dest, src, n);
}

void __aeabi_memcpy4(void *dest, const void *src, size_t n)
{
	rt_memcpy(dest, src, n);
}

//void *memset(void *src, int c, size_t n)
//{
//	return rt_memset(src, c, n);
//}

//void *memcpy(void *dest, const void *src, size_t n)
//{
//	return rt_memcpy(dest, src, n);
//}

