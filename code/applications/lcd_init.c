#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

#include <rtthread.h>
#include <rtdevice.h>
#include "board.h"
#include <rthw.h>

#include "drv_sram.h"
#include "lcd_port.h"

//#define LCD_CMD_REG  *((__IO uint16_t *) ((uint32_t)(0X6C000000 | 0x0000007E )))
//#define LCD_DATA_REG  *((__IO uint16_t *) ((uint32_t)(0X6C000000 | 0x00000080 )))

//-----------------MCU屏 LCD端口定义---------------- 
////LCD地址结构体
//typedef struct
//{
//	volatile uint16_t LCD_REG;
//	volatile uint16_t LCD_RAM;
//} LCD_TypeDef;
////使用NOR/SRAM的 Bank1.sector4,地址位HADDR[27,26]=11 A6作为数据命令区分线 
////注意设置时STM32内部会右移一位对其! 111 1110=0X7E			    
////#define LCD_BASE        ((uint32_t)(0x6C000000 | 0x0000007E))
//#define LCD_BASE        ((uint32_t)(0x6C000000 | 0x0000007E))
//#define LCD             ((LCD_TypeDef *) LCD_BASE)

//#define LCD_CMD_REG  LCD->LCD_REG
//#define LCD_DATA_REG  LCD->LCD_RAM


//void WriteComm(uint16_t CMD) //写命令
//{
//	rt_base_t level;
//	/* disable interrupt */
//	level = rt_hw_interrupt_disable();
//	
//	LCD_CMD_REG = CMD;
//	
//	/* enable interrupt */
//	rt_hw_interrupt_enable(level);
//}

//void WriteData(uint16_t data) //写数据
//{
//	rt_base_t level;
//	/* disable interrupt */
//	level = rt_hw_interrupt_disable();
//	
//	LCD_DATA_REG = data;
//	
//	/* enable interrupt */
//	rt_hw_interrupt_enable(level);
//}
///******************************************
//函数名：  Lcd写命令函数
//功能：    向Lcd指定位置写入应有命令或数据
//入口参数：LCD_Reg 要寻址的寄存器地址
//          LCD_RegValue 写入的数据或命令值
//******************************************/
//void LCD_WR_REG(uint16_t LCD_Reg,uint16_t LCD_RegValue)
//{
//	rt_base_t level;
//	/* disable interrupt */
//	level = rt_hw_interrupt_disable();
//	
//	LCD_CMD_REG = LCD_Reg;		//写入要写的寄存器序号	 
//	LCD_DATA_REG = LCD_RegValue;//写入数据	
//	
//	/* enable interrupt */
//	rt_hw_interrupt_enable(level);
//}

////LCD写GRAM
////RGB_Code:颜色值
//void LCD_WriteRAM(uint16_t RGB_Code)
//{							    
//	LCD_DATA_REG = RGB_Code;//写十六位GRAM
//}



#define Bank1_LCD_D    ((uint32_t)0x6C000000 | 0x00000080)    //Disp Data ADDR
#define Bank1_LCD_C    ((uint32_t)(0x6C000000 | 0x0000007E))	   //Disp Reg ADDR

 void WriteComm(uint16_t CMD)
{			
	*(__IO uint16_t *) (Bank1_LCD_C) = CMD;
}
 void WriteData(uint16_t tem_data)
{			
	*(__IO uint16_t *) (Bank1_LCD_D) = tem_data;
}


/******************************************
函数名：  Lcd写命令函数
功能：    向Lcd指定位置写入应有命令或数据
入口参数：LCD_Reg 要寻址的寄存器地址
          LCD_RegValue 写入的数据或命令值
******************************************/
void LCD_WR_REG(uint16_t LCD_Reg,uint16_t LCD_RegValue)
{
//	rt_base_t level;
//	/* disable interrupt */
//	level = rt_hw_interrupt_disable();
	
	
	*(__IO uint16_t *) (Bank1_LCD_C) = LCD_Reg;
	*(__IO uint16_t *) (Bank1_LCD_D) = LCD_RegValue;
	
//	LCD->LCD_REG = LCD_Reg;     //写入要写的寄存器序号
//	LCD->LCD_RAM = LCD_RegValue;//写入数据
	
//	/* enable interrupt */
//	rt_hw_interrupt_enable(level);
}


void hw_lcd_delay(int time)  //简单软件 延时函数
{
//	uint32_t i,j;
//	for(i=0;i<time;i++)
//		for(j=0;j<100;j++);
	
	rt_thread_mdelay(time);
}		

void Lcd_SetCursor(uint16_t x,uint16_t y)
{ 
	LCD_WR_REG(0x20,y);
	LCD_WR_REG(0x21,x);
}

/**********************************************
函数名：开窗函数

入口参数：XStart x方向的起点
          Xend   x方向的终点
					YStart y方向的起点
          Yend   y方向的终点

这个函数的意义是：开一个矩形框，方便接下来往这个框填充数据
***********************************************/
void hw_lcd_block(uint16_t Xstart,uint16_t Xend,uint16_t Ystart,uint16_t Yend)
{
	//x与y交换
	LCD_WR_REG(0x0046,(Yend<<8)|Ystart);//水平GRAM终止，起始位置
	LCD_WR_REG(0x0047,Xend);//垂直GRAM终止位置
	LCD_WR_REG(0x0048,Xstart);//垂直 GRAM起始位置
	
//	LCD_WR_REG(0x20,Ystart);//水平坐标
//	LCD_WR_REG(0x21,Xstart);//垂直坐标  
	Lcd_SetCursor(Xstart, Ystart);
	
  WriteComm(0x022);
}
/**********************************************
函数名：Lcd矩形填充函数

入口参数：xStart x方向的起始点
          ySrart y方向的终止点
          xLong 要选定矩形的x方向长度
          yLong  要选定矩形的y方向长度
返回值：无
***********************************************/
void hw_lcd_box(uint16_t xStart,uint16_t yStart,uint16_t xLen,uint16_t yLen,lcd_color_t Color)
{
	hw_lcd_block(xStart,xStart+xLen-1,yStart,yStart+yLen-1);
	uint16_t y_index = 0;
	uint16_t x_index = 0;
	
	while(y_index < yLen)
	{
		x_index = 0;
		Lcd_SetCursor(xStart, yStart + y_index);
		WriteComm(0x022);
		while(x_index < xLen)
		{
			x_index ++;
			WriteData(Color);
		}
		y_index ++;
	}
}
//=============== 在x，y 坐标上打一个颜色为Color的点 ===============
void hw_lcd_pixel(uint16_t x,uint16_t y, lcd_color_t Color)
{
	hw_lcd_block(x,x,y,y);
	WriteData(Color);
}
/******************************************
函数名：Lcd图像填充
功能：向Lcd指定位置填充图像
入口参数：
					(x,y): 图片左上角起始坐标
					(pic_H,pic_V): 图片的宽高
					 pic  指向存储图片数组的指针
******************************************/
void hw_lcd_flush(uint16_t xStart,uint16_t yStart,uint16_t xLen,uint16_t yLen,lcd_color_t *buff)
{
	//DMA 方式
	hw_lcd_block(xStart,xStart+xLen-1, yStart ,yStart+yLen-1);
	//while()
	uint16_t y_index = 0;
	uint16_t x_index = 0;
	while(y_index < yLen)
	{
		x_index = 0;
		Lcd_SetCursor(xStart, yStart + y_index);
		WriteComm(0x022);
		while(x_index < xLen)
		{
			x_index ++;
			WriteData(*buff);
			
			buff++;
		}
		y_index ++;
	}
}


void hw_lcd_fill_pic(uint16_t xStart,uint16_t yStart,uint16_t xLen,uint16_t yLen,lcd_color_t *buff)
{
	//DMA 方式
	hw_lcd_block(xStart,xStart+xLen-1, yStart ,yStart+yLen-1);
	//while()
	uint16_t y_index = 0;
	uint16_t x_index = 0;
	while(y_index < yLen)
	{
		x_index = 0;
		Lcd_SetCursor(xStart, yStart + y_index);
		WriteComm(0x022);
		while(x_index < xLen)
		{
			x_index ++;
			WriteData(*buff);
			
			buff++;
		}
		y_index ++;
	}
}

void lcd_cmd_btg242432_init(void);

void hw_lcd_initial(void) //LCD初始化函数
{
	GPIO_InitTypeDef GPIO_Initure;
    
	__HAL_RCC_GPIOB_CLK_ENABLE();			//开启GPIOB时钟
	GPIO_Initure.Pin=GPIO_PIN_15;          	//PB15,背光控制
	GPIO_Initure.Mode=GPIO_MODE_OUTPUT_PP;  //推挽输出
	GPIO_Initure.Pull=GPIO_PULLUP;          //上拉
	GPIO_Initure.Speed=GPIO_SPEED_HIGH;     //高速
	HAL_GPIO_Init(GPIOB,&GPIO_Initure); 
	
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_15,GPIO_PIN_SET);
	
	hw_lcd_delay(10);
	
	lcd_cmd_btg242432_init();
	
	hw_lcd_delay(10);
	
	hw_lcd_box(0,0,100,100,BLUE);
}

//#define LCD_delay lcd_delay_ms
#define LCD_delay(n)  hw_lcd_delay(n)

void lcd_cmd_btg242432_init(void)
{
	LCD_WR_REG(0x11,0x2004);		
	LCD_WR_REG(0x13,0xCC00);		
	LCD_WR_REG(0x15,0x2600);	
	LCD_WR_REG(0x14,0x252A);	
	//	LCD_WR_REG(0x14,0x002A);		
	LCD_WR_REG(0x12,0x0033);		
	LCD_WR_REG(0x13,0xCC04);		
	LCD_delay(1); 
	LCD_WR_REG(0x13,0xCC06);		
	LCD_delay(1); 
	LCD_WR_REG(0x13,0xCC4F);		
	LCD_delay(1); 
	LCD_WR_REG(0x13,0x674F);
	LCD_WR_REG(0x11,0x2003);
	LCD_delay(1); 	
	LCD_WR_REG(0x30,0x2609);		
	LCD_WR_REG(0x31,0x242C);		
	LCD_WR_REG(0x32,0x1F23);		
	LCD_WR_REG(0x33,0x2425);		
	LCD_WR_REG(0x34,0x2226);		
	LCD_WR_REG(0x35,0x2523);		
	LCD_WR_REG(0x36,0x1C1A);		
	LCD_WR_REG(0x37,0x131D);		
	LCD_WR_REG(0x38,0x0B11);		
	LCD_WR_REG(0x39,0x1210);		
	LCD_WR_REG(0x3A,0x1315);		
	LCD_WR_REG(0x3B,0x3619);		
	LCD_WR_REG(0x3C,0x0D00);		
	LCD_WR_REG(0x3D,0x000D);		
	LCD_WR_REG(0x16,0x0007);		
	LCD_WR_REG(0x02,0x0013);		
	LCD_WR_REG(0x03,0x000B);		
	LCD_WR_REG(0x01,0x1027);		
	LCD_delay(1); 
	LCD_WR_REG(0x08,0x0303);		
	LCD_WR_REG(0x0A,0x000B);		
	LCD_WR_REG(0x0B,0x0003);   
	LCD_WR_REG(0x0C,0x0000);   
	LCD_WR_REG(0x41,0x0000);    
	LCD_WR_REG(0x50,0x0000);   
	LCD_WR_REG(0x60,0x0005);    
	LCD_WR_REG(0x70,0x000B);    
	LCD_WR_REG(0x71,0x0000);    
	LCD_WR_REG(0x78,0x0000);    
	LCD_WR_REG(0x7A,0x0000);   
	LCD_WR_REG(0x79,0x0007);		
	LCD_WR_REG(0x07,0x0051);   
	LCD_delay(1); 	
	LCD_WR_REG(0x07,0x0053);		
	LCD_WR_REG(0x79,0x0000);
	WriteComm(0x22);  
}

//void lcd_cmd_btg242432_init(void)
//{
//	LCD_WR_REG(0x11,0x2004);		
//	LCD_WR_REG(0x13,0xCC00);		
//	LCD_WR_REG(0x15,0x2600);	
//	LCD_WR_REG(0x14,0x252A);	
//	//	LCD_WR_REG(0x14,0x002A);		
//	LCD_WR_REG(0x12,0x0033);		
//	LCD_WR_REG(0x13,0xCC04);		
//	LCD_delay(1); 
//	LCD_WR_REG(0x13,0xCC06);		
//	LCD_delay(1); 
//	LCD_WR_REG(0x13,0xCC4F);		
//	LCD_delay(1); 
//	LCD_WR_REG(0x13,0x674F);
//	LCD_WR_REG(0x11,0x2003);
//	LCD_delay(1); 	
//	LCD_WR_REG(0x30,0x2609);		
//	LCD_WR_REG(0x31,0x242C);		
//	LCD_WR_REG(0x32,0x1F23);		
//	LCD_WR_REG(0x33,0x2425);		
//	LCD_WR_REG(0x34,0x2226);		
//	LCD_WR_REG(0x35,0x2523);		
//	LCD_WR_REG(0x36,0x1C1A);		
//	LCD_WR_REG(0x37,0x131D);		
//	LCD_WR_REG(0x38,0x0B11);		
//	LCD_WR_REG(0x39,0x1210);		
//	LCD_WR_REG(0x3A,0x1315);		
//	LCD_WR_REG(0x3B,0x3619);		
//	LCD_WR_REG(0x3C,0x0D00);		
//	LCD_WR_REG(0x3D,0x000D);		
//	LCD_WR_REG(0x16,0x0007);		
//	LCD_WR_REG(0x02,0x0013);		
//	LCD_WR_REG(0x03,0x000B); //---Entry Mode		
//	LCD_WR_REG(0x01,0x1027);	//Driver Output Control (240 x 320)	
//	LCD_delay(1); 
//	LCD_WR_REG(0x08,0x0303);		
//	LCD_WR_REG(0x0A,0x000B);		
//	LCD_WR_REG(0x0B,0x0003);   
//	LCD_WR_REG(0x0C,0x0000);   
//	LCD_WR_REG(0x41,0x0000);    
//	LCD_WR_REG(0x50,0x0000);   
//	LCD_WR_REG(0x60,0x0005);    
//	LCD_WR_REG(0x70,0x000B);    
//	LCD_WR_REG(0x71,0x0000);    
//	LCD_WR_REG(0x78,0x0000);    
//	LCD_WR_REG(0x7A,0x0000);   
//	LCD_WR_REG(0x79,0x0007);		
//	LCD_WR_REG(0x07,0x0051);   
//	LCD_delay(1); 	
//	LCD_WR_REG(0x07,0x0053);	//Display Control 1(disable 8-color mode,display on,normal)   	
//	LCD_WR_REG(0x79,0x0000);
//	WriteComm(0x22);  
////	LCD_delay(1); 
////	LCD_WR_REG(0x07, 0x0173);
////	WriteComm(0x22); 
////	//Set_address_mode
////	LCD_WR_REG(0x01,0x0127);
//// 	LCD_WR_REG(0x03,0x000A); //横屏，从左下角开始，从左到右，从下到上
//	
////	//Set_address_mode
////	LCD_WR_REG(0x01,0x1027);
//// 	LCD_WR_REG(0x03,0x010A); //横屏，从右上角开始，从左到右，从上到下

////	WriteComm(0x22);  
//}

