#ifndef _BOARD_SYSTEM_H_
#define _BOARD_SYSTEM_H_

#include "stdint.h"
#include <rtthread.h>
#include <rtdevice.h>

int board_led_set(int index, int status);
int board_key_get(int index);

char *board_unique_id_str(void);


// void board_load_default_mac(uint8_t* mac_addr);

//#include "board_config.h"

//userconfig_system_t * board_system_get(void);

//char *board_hostname(void);

//char *board_version(void);


void rt_hw_us_delay(rt_uint32_t us);

void delay_us(uint32_t us);

void delay_ms(uint32_t ms);

uint32_t tick_get(void);

#endif
